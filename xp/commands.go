package xp

import (
	"fmt"
	"log"
	"regexp"
	"sort"
	"strings"

	dg "github.com/bwmarrin/discordgo"
)

// AutoRankFile is the default file for loading and saving auto promote ranks
const AutoRankFile string = "data/autoRanks.json"

// MsgEmbed is used to shorten the name of the original embed type from discordGo
type MsgEmbed *dg.MessageEmbed

// Execute is the method used to run the correct method based on user input.
func (x *System) Execute(req []string, msg *dg.MessageCreate) (MsgEmbed, error) {
	// When user runs the xp command with alone return that user's XP
	if len(req) < 2 {
		return x.returnXp(req, msg)
	}

	switch req[1] {
	case "save":
		// DefaultFile is declared in xp/xp.go
		if err := x.saveXP(XpFile); err != nil {
			return nil, err
		}
		return &dg.MessageEmbed{Description: "XP data saved!"}, nil
	case "lb":
		return x.leaderBoard(msg)
	}
	return x.returnXp(req, msg)
}

// ReturnXp checks the user's request and returns xp data based on the command entered.
func (x *System) returnXp(req []string, msg *dg.MessageCreate) (MsgEmbed, error) {
	// When user runs the xp command with alone return that user's XP
	if len(req) < 2 {
		return x.userXp(msg.Author.Username, msg.Author.ID, msg)
	}

	id, err := x.findUserID(req[1], msg)
	if err != nil {
		return nil, fmt.Errorf("xp findUserID failed: %s", err)
	}

	member, err := x.dgs.GuildMember(msg.GuildID, id)
	if err != nil {
		return nil, fmt.Errorf("xp GuildMember failed: %s", err)
	}

	return x.userXp(member.User.Username, id, msg)
}

func (x *System) userXp(name, userID string, msg *dg.MessageCreate) (MsgEmbed, error) {
	alertUser, tooSoon := x.callRec.CheckLastAsk(msg)
	if tooSoon {
		return &dg.MessageEmbed{Description: alertUser}, nil
	}

	xp, ok := x.Data.Users[userID]
	if !ok {
		return &dg.MessageEmbed{Description: fmt.Sprintf("%s has not earned any XP", name)}, nil
	}

	return &dg.MessageEmbed{Description: fmt.Sprintf("%s has a total of %.2f xp", name, xp)}, nil
}

// Currently not used; needs a way to look up nicknames before checking the actual
// usernames to avoid returning incorrect data.
func (x *System) findUserID(userName string, msg *dg.MessageCreate) (string, error) {
	var (
		id      string
		members []*dg.Member
		err     error
	)

	var re = regexp.MustCompile("(?m)^<@!\\w+>")
	match := re.MatchString(userName)
	if match {
		id = strings.TrimPrefix(userName, "<@!")
		id = strings.TrimSuffix(id, ">")
		return id, nil
	}

	// Get first round of members
	current, err := x.dgs.GuildMembers(msg.GuildID, "", 1000)
	if err != nil {
		return "", err
	}

	for _, names := range current {
		members = append(members, names)
	}

	// if first round has 1000 entries run again until all members are present.
	for len(current) == 1000 {
		lastMember := current[len(current)-1]
		current, err = x.dgs.GuildMembers(msg.GuildID, lastMember.User.ID, 1000)
		if err != nil {
			return "", fmt.Errorf("GuildMembers failed: %s", err)
		}

		for _, names := range current {
			members = append(members, names)
		}

	}

	for _, member := range members {
		if member.Nick == userName {
			id = member.User.ID
			break
		}

		if member.User.Username == userName {
			id = member.User.ID
			break
		}
	}

	return id, nil
}

func (x *System) leaderBoard(msg *dg.MessageCreate) (MsgEmbed, error) {
	flippedMap := make(map[float64]string)
	xpSlice := []float64{}
	for id, xp := range x.Data.Users {
		xpSlice = append(xpSlice, xp)
		flippedMap[xp] = id
	}

	sort.Float64s(xpSlice)

	totalUsers := len(xpSlice) - 1
	topStop := totalUsers - 10

	// Check to make sure topStop is not a negitive number
	if totalUsers < 10 {
		topStop = 0
	}

	var top10 string
	rank := 1
	for i := totalUsers; i >= topStop; i-- {
		userID := flippedMap[xpSlice[i]]

		user, err := x.dgs.GuildMember(msg.GuildID, userID)
		if err != nil {
			log.Printf("leaderboard() GuildMember() returned an error: %s", err)
			continue
		}

		top10 = fmt.Sprintf("%s\n%d) %s (%.2f)", top10, rank, user.User.Username, xpSlice[i])
		rank++
	}

	embed := &dg.MessageEmbed{
		Title:       "Top 10",
		Description: top10,
	}

	return embed, nil
}
